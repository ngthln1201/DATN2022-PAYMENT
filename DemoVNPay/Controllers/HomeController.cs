﻿using DATN2022.Models;

using DemoVNPay.Models;
using DemoVNPay.Others;
using Newtonsoft.Json;

using StackExchange.Redis;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DemoVNPay.Controllers
{
    public class HomeController : Controller
    {
        static readonly ConnectionMultiplexer _redis = ConnectionMultiplexer.Connect("redis-18398.c9.us-east-1-4.ec2.cloud.redislabs.com:18398,password=datn2022@");
        public ActionResult Index(CreateBookingRequestModel input)
        {
            var db = _redis.GetDatabase(0);
            var datajson = db.StringGet("ticket").ToString();
            var result = JsonConvert.DeserializeObject<CreateBookingRequestModel>(datajson);
            return View(result);
        }

        public  ActionResult Payment()
        {
            var db = _redis.GetDatabase(0);
            var datajson = db.StringGet("ticket").ToString();
            var result = JsonConvert.DeserializeObject<CreateBookingRequestModel>(datajson);
            
            string url = ConfigurationManager.AppSettings["Url"];
            string returnUrl = ConfigurationManager.AppSettings["ReturnUrl"];
            string tmnCode = ConfigurationManager.AppSettings["TmnCode"];
            string hashSecret = ConfigurationManager.AppSettings["HashSecret"];

            PayLib pay = new PayLib();

            pay.AddRequestData("vnp_Version", "2.1.0"); //Phiên bản api mà merchant kết nối. Phiên bản hiện tại là 2.1.0
            pay.AddRequestData("vnp_Command", "pay"); //Mã API sử dụng, mã cho giao dịch thanh toán là 'pay'
            pay.AddRequestData("vnp_TmnCode", tmnCode); //Mã website của merchant trên hệ thống của VNPAY (khi đăng ký tài khoản sẽ có trong mail VNPAY gửi về)
            pay.AddRequestData("vnp_Amount", result.TotalPaymentAmount == 0 ? "1000000" : (result.TotalPaymentAmount * 100).ToString()); //số tiền cần thanh toán, công thức: số tiền * 100 - ví dụ 10.000 (mười nghìn đồng) --> 1000000
            pay.AddRequestData("vnp_BankCode", ""); //Mã Ngân hàng thanh toán (tham khảo: https://sandbox.vnpayment.vn/apis/danh-sach-ngan-hang/), có thể để trống, người dùng có thể chọn trên cổng thanh toán VNPAY
            pay.AddRequestData("vnp_CreateDate", DateTime.Now.ToString("yyyyMMddHHmmss")); //ngày thanh toán theo định dạng yyyyMMddHHmmss
            pay.AddRequestData("vnp_CurrCode", "VND"); //Đơn vị tiền tệ sử dụng thanh toán. Hiện tại chỉ hỗ trợ VND
            pay.AddRequestData("vnp_IpAddr", Util.GetIpAddress()); //Địa chỉ IP của khách hàng thực hiện giao dịch
            pay.AddRequestData("vnp_Locale", "vn"); //Ngôn ngữ giao diện hiển thị - Tiếng Việt (vn), Tiếng Anh (en)
            pay.AddRequestData("vnp_OrderInfo", "Thanh toan don hang"); //Thông tin mô tả nội dung thanh toán
            pay.AddRequestData("vnp_OrderType", "other"); //topup: Nạp tiền điện thoại - billpayment: Thanh toán hóa đơn - fashion: Thời trang - other: Thanh toán trực tuyến
            pay.AddRequestData("vnp_ReturnUrl", returnUrl); //URL thông báo kết quả giao dịch khi Khách hàng kết thúc thanh toán
            pay.AddRequestData("vnp_TxnRef", DateTime.Now.Ticks.ToString()); //mã hóa đơn

            string paymentUrl = pay.CreateRequestUrl(url, hashSecret);

            return Redirect(paymentUrl);
        }

        public async Task<ActionResult> PaymentConfirm()
        {
            if (Request.QueryString.Count > 0)
            {
                string hashSecret = ConfigurationManager.AppSettings["HashSecret"]; //Chuỗi bí mật
                var vnpayData = Request.QueryString;
                PayLib pay = new PayLib();

                //lấy toàn bộ dữ liệu được trả về
                foreach (string s in vnpayData)
                {
                    if (!string.IsNullOrEmpty(s) && s.StartsWith("vnp_"))
                    {
                        pay.AddResponseData(s, vnpayData[s]);
                    }
                }

                long orderId = Convert.ToInt64(pay.GetResponseData("vnp_TxnRef")); //mã hóa đơn
                long vnpayTranId = Convert.ToInt64(pay.GetResponseData("vnp_TransactionNo")); //mã giao dịch tại hệ thống VNPAY
                string vnp_ResponseCode = pay.GetResponseData("vnp_ResponseCode"); //response code: 00 - thành công, khác 00 - xem thêm https://sandbox.vnpayment.vn/apis/docs/bang-ma-loi/
                string vnp_SecureHash = Request.QueryString["vnp_SecureHash"]; //hash của dữ liệu trả về

                bool checkSignature = pay.ValidateSignature(vnp_SecureHash, hashSecret); //check chữ ký đúng hay không?


                if (checkSignature)
                {
                    if (vnp_ResponseCode == "00")
                    {
                        //Thanh toán thành công
                        ViewBag.Message = "Thanh toán thành công hóa đơn " + orderId + " | Mã giao dịch: " + vnpayTranId;
                        var dt = pay.GetResponseData("vnp_CreateDate");

                        //var order = new OrderModel
                        //{
                        //    Id = Guid.NewGuid(),
                        //    OrderNo = orderId,
                        //    Amount = Convert.ToInt64(pay.GetResponseData("vnp_Amount")),
                        //    OrderDesc = pay.GetResponseData("vnp_OrderInfo"),
                        //    //CreatedDate = DateTime.ParseExact(pay.GetResponseData("vnp_CreateDate"),
                        //    //    "yyyyMMddHHmmss",
                        //    //    System.Globalization.CultureInfo.InvariantCulture),
                        //    Status = pay.GetResponseData("vnp_ReturnUrl"),
                        //    PaymentTranId = vnpayTranId,
                        //    BankCode = pay.GetResponseData("vnp_BankCode"),
                        //    PayStatus = vnp_ResponseCode,
                        //};
                        //if (!order.Equals(null))
                        //{
                        //    var db = _redis.GetDatabase(0);
                        //    var totalAmount = db.StringSet("ordermodel", JsonConvert.SerializeObject(order));
                        //}

                        var httpClient = new HttpClient();
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:44332/api/Booking/save-to-database");
                        var response = await httpClient.SendAsync(request);

                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            return ViewBag.Message = "Có lỗi xảy ra";
                        }

                    }
                    else
                    {
                        //Thanh toán không thành công. Mã lỗi: vnp_ResponseCode
                        ViewBag.Message = "Có lỗi xảy ra trong quá trình xử lý hóa đơn " + orderId + " | Mã giao dịch: " + vnpayTranId + " | Mã lỗi: " + vnp_ResponseCode;
                    }
                }
                else
                {
                    ViewBag.Message = "Có lỗi xảy ra trong quá trình xử lý";
                }
            }

            return View();
        }
    }
}
