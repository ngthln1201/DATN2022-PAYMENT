﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DemoVNPay.Models
{
    public class CreateBookingRequestModel
    {
        public Guid Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public Guid IdPickup { get; set; }
        public Guid IdDropOff { get; set; }
        public int TotalPaymentAmount { get; set; }
        
        public List<CreateBookingDetail> Details { get; set; }
    }
    public class CreateBookingDetail
    {
        public Guid Id { get; set; }
        public int SeatNo { get; set; }

    }
}